const info = () => ({
    name: 'Syge fuglen',
    team: 'Gruppe 8'
});

const getBody = request => {
    switch (request.query.path) {
        case '/info':
            return clientInformation()
    }
}

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    context.res = {
        body: getBody(req)
    }
    context.done();
};